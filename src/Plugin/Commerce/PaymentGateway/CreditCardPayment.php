<?php

namespace Drupal\commerce_ecpay_allinone\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

include '../vendor/drupal-commerce-library/aiosdk/sdk/ECPay.Payment.Integration.php';
/**
 * Provides the ECPay Credit Card offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "ecpay_credit_card",
 *   label = @Translation("ECPay Credit Card"),
 *   display_label = @Translation("Credit Card"),
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_ecpay_allinone\PluginForm\CreditCardRedirectCheckoutForm",
 *   },
 * )
 */
class CreditCardPayment extends OffsitePaymentGatewayBase implements CreditCardPaymentInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new OffsitePayPlug object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->get('entity_type.manager'),
          $container->get('plugin.manager.commerce_payment_type'),
          $container->get('plugin.manager.commerce_payment_method_type'),
          $container->get('datetime.time')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'merchantId' => '',
      'hashKey' => '',
      'hashIv' => '',
      'tradeDesc' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['merchantId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#description' => $this->t('For development purpose, you can use 2000132 instead.'),
      '#default_value' => $this->configuration['merchantId'],
      '#required' => TRUE,
    ];

    $form['hashKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hash Key'),
      '#description' =>
      $this->t('For development purpose, you can use 5294y06JbISpM5x9 instead.'),
      '#default_value' => $this->configuration['hashKey'],
      '#required' => TRUE,
    ];

    $form['hashIv'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hash IV'),
      '#description' => $this->t('For development purpose, you can use v77hoKGq4kWxNNIS instead.'),
      '#default_value' => $this->configuration['hashIv'],
      '#required' => TRUE,
    ];

    $form['tradeDesc'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Trade Description'),
      '#description' => $this->t('Description is needed for ECPay payment process.'),
      '#default_value' => $this->configuration['tradeDesc'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['merchantId'] = $values['merchantId'];
      $this->configuration['hashKey'] = $values['hashKey'];
      $this->configuration['hashIv'] = $values['hashIv'];
      $this->configuration['tradeDesc'] = $values['tradeDesc'];
    }
  }

  /**
   * {@inheritDoc}
   */
  public function onNotify(Request $request) {

    try {
      $obj = new \ECPay_AllInOne();
      $obj->HashKey = $this->configuration['hashKey'];
      $obj->HashIV = $this->configuration['hashIv'];
      $obj->MerchantID = $this->configuration['merchantId'];
      // SHA256.
      $obj->EncryptType = \ECPay_EncryptType::ENC_SHA256;
      $feedback = $obj->CheckOutFeedback();

      $merchantTradeNo = $feedback['MerchantTradeNo'];
      $paymentId = substr($merchantTradeNo, 14);
      $payment = $this->entityTypeManager->getStorage('commerce_payment')->load($paymentId);
      $payment_state = $payment->getState();
      $payment_state_transition = $payment_state->getTransitions();
      $payment->set('remote_id', $feedback['TradeNo']);
      $payment->set('remote_state', $feedback['RtnMsg']);

      // It's simulated paid, but seems don't need at credit card payment.
      if ($feedback['SimulatePaid']) {
        throw new \Exception('Simulate Paid, got the data came from ECPay: @data', ['@data' => [$feedback]]);
      }
      if ($feedback['RtnCode'] == 1) {
        $payment_state->applyTransition($payment_state_transition['capture']);
      }
      else {
        $payment_state->applyTransition($payment_state_transition['void']);
      }
      $payment->save();

      return new Response('1|OK', 200, [
        'Content-Type' => 'text/plain',
      ]);
    }
    catch (\Exception $e) {
      \Drupal::logger('commerce_ecpay_allinone')->error('Error onNotify page. Return Message: @error',
        ['@error' => $e->getMessage()]
      );
      return new Response('0|' . $e->getMessage(), 200, [
        'Content-Type' => 'text/plain',
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    // TODO: The Client comes back action.
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
  }

}
