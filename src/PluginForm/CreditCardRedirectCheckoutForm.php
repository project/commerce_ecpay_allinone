<?php

namespace Drupal\commerce_ecpay_allinone\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * This is the redirect form lead the user to payment service.
 */
class CreditCardRedirectCheckoutForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $host = \Drupal::request()->getSchemeAndHttpHost();

    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $ecpayPlugin = $payment->getPaymentGateway()->getPlugin();
    $mode = $ecpayPlugin->getMode();
    $configuration = $ecpayPlugin->getConfiguration();
    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = $payment->getOrder();

    try {
      // Place Order.
      $order->getState()->applyTransitionById('place');
      $order->save();
      // Eastablish ECPay Connection.
      $obj = new \ECPay_AllInOne();
      $obj->HashKey = $configuration['hashKey'];
      $obj->HashIV = $configuration['hashIv'];
      $obj->MerchantID = $configuration['merchantId'];
      $obj->EncryptType = '1';
      $obj->Send['ChoosePayment'] = \ECPay_PaymentMethod::Credit;
      if ($mode == "live") {
        $obj->ServiceURL = "https://payment.ecpay.com.tw/Cashier/AioCheckOut/V5";
      }
      else {
        $obj->ServiceURL = "https://payment-stage.ecpay.com.tw/Cashier/AioCheckOut/V5";
      }

      // Establish payment entity.
      $payment->set('state', 'authorization');
      $payment->save();
      $merchantTradeNo = date("YmdHis") . $payment->id();

      // Add order items to the sending object.
      $items = $order->getItems();
      $sendItems = [];
      foreach ($items as $item) {
        /** @var \Drupal\commerce_order\Entity\OrderItem $item */
        $itemName = $item->getTitle();
        $itemQuantity = (int) $item->getQuantity();
        $purchasedUrl = $item->getPurchasedEntity()->toUrl()->toString();
        $sendItems[] = [
          'Name' => $itemName,
          'Price' => (int) $item->getTotalPrice()->getNumber(),
          'Currency' => $item->getTotalPrice()->getCurrencyCode(),
          'Quantity' => $itemQuantity,
          'URL' => $host . $purchasedUrl,
        ];
      }

      $obj->Send['Items'] = $sendItems;
      $obj->Send['ReturnURL'] = $ecpayPlugin->getNotifyUrl()->toString();
      $obj->Send['ClientBackURL'] = $form['#return_url'];
      $obj->Send['MerchantTradeNo'] = $merchantTradeNo;
      $obj->Send['MerchantTradeDate'] = date('Y/m/d H:i:s');
      $obj->Send['TotalAmount'] = (int) $order->getTotalPrice()->getNumber();
      $obj->Send['TradeDesc'] = $configuration['tradeDesc'];
      $obj->CheckOut();
    }
    catch (\Execption $e) {
      \Drupal::logger('commerce_ecpay_allinone')->error($e->getMessage());
    }

  }

}
